package alterarsenha;

import base.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AlterarSenhaPage {


    private WebDriver driver;

    public AlterarSenhaPage(WebDriver driver){
        this.driver= driver;
    }

    private WebElement getLoginTxt() {
        return driver.findElement(By.id("login"));
    }

    private WebElement getSenhaAtualTxt() {
        return driver.findElement(By.id("senhaatual"));
    }

    private WebElement getDataNascimentoTxt() {
        return driver.findElement(By.id("datanascimento"));
    }

    private WebElement getCPFTxt() {
        return driver.findElement(By.id("cpf"));
    }

    private WebElement getSenhaNovaTxt() {
        return driver.findElement(By.id("senhanova"));
    }

    private WebElement getConfirmeSenhaTxt() {
        return driver.findElement(By.id("confirmasenha"));
    }

    private WebElement getLembreTxt() {
        return driver.findElement(By.id("lembrete"));
    }

    private WebElement getAlterarSenhaBtn() {
        return driver.findElement(By.id("button"));
    }

    public void clicarEmAlterarSenha(){
        this.getAlterarSenhaBtn().click();
    }

    public void preencherCampoLogin(String login){
        this.getLoginTxt().sendKeys(login);
    }

    public void preencherCampoSenhaAtual(String senhaAtual){
        this.getSenhaAtualTxt().sendKeys(senhaAtual);
    }

    public void preencherCampoDataNascimento(String dataNascimento){
        this.getDataNascimentoTxt().sendKeys(dataNascimento);
    }

    public void preencherCampoCPF(String cpf){
        this.getCPFTxt().sendKeys(cpf);
    }

    public void preencherCampoSenhaNova(String senhaNova){
        this.getSenhaNovaTxt().sendKeys(senhaNova);
    }

    public void preencherCampoConfirmeSenha(String confirmeSenha){
        this.getConfirmeSenhaTxt().sendKeys(confirmeSenha);
    }

    public void preencherCampoLembrete(String lembrete){
        this.getLembreTxt().sendKeys(lembrete);
    }

    public void acessar(){
        this.driver.get("file:///"+ Util.workDirectory()+"/PaginaAlterarSenha/login.html");
    }

    public String getMensagemAlerta(){
        return this.driver.switchTo().alert().getText();
    }

}
