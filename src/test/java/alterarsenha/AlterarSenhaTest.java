package alterarsenha;

import base.BaseTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;


public class AlterarSenhaTest extends BaseTest {

    private AlterarSenhaPage alterarSenhaPage;

    public AlterarSenhaTest() {
        super( new ChromeDriver());;
    }

    @Before
    public void setup(){
        this.basicSetup();
        this.alterarSenhaPage = new AlterarSenhaPage(this.driver);
    }

    @After
    public void tearDown(){
        this.basicTearDown();
    }

    @Test
    public void testAlteracaoComSucesso(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("rafael.melo");
        alterarSenhaPage.preencherCampoSenhaAtual("12345");
        alterarSenhaPage.preencherCampoCPF("18773103012");
        alterarSenhaPage.preencherCampoDataNascimento("01101988");
        alterarSenhaPage.preencherCampoSenhaNova("1234567");
        alterarSenhaPage.preencherCampoConfirmeSenha("1234567");
        alterarSenhaPage.preencherCampoLembrete("Cachorro");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("Alteração realizada com sucesso!",alterarSenhaPage.getMensagemAlerta());
    }

    @Test
    public void testLembreteVazio(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("rafael.melo");
        alterarSenhaPage.preencherCampoSenhaAtual("12345");
        alterarSenhaPage.preencherCampoCPF("18773103012");
        alterarSenhaPage.preencherCampoDataNascimento("01101988");
        alterarSenhaPage.preencherCampoSenhaNova("1234567");
        alterarSenhaPage.preencherCampoConfirmeSenha("1234567");
        alterarSenhaPage.preencherCampoLembrete("");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("1 - Lembrete inválido",alterarSenhaPage.getMensagemAlerta());
    }

    @Test
    public void testCPFDigitoInvalido(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("rafael.melo");
        alterarSenhaPage.preencherCampoSenhaAtual("12345");
        alterarSenhaPage.preencherCampoCPF("18773103014");
        alterarSenhaPage.preencherCampoDataNascimento("01101988");
        alterarSenhaPage.preencherCampoSenhaNova("1234567");
        alterarSenhaPage.preencherCampoConfirmeSenha("1234567");
        alterarSenhaPage.preencherCampoLembrete("Cachorro");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("1 - Dígito verificador inválido",alterarSenhaPage.getMensagemAlerta());
    }


    @Test
    public void testSenhaNovaDiferente(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("rafael.melo");
        alterarSenhaPage.preencherCampoSenhaAtual("12345");
        alterarSenhaPage.preencherCampoCPF("18773103012");
        alterarSenhaPage.preencherCampoDataNascimento("01101988");
        alterarSenhaPage.preencherCampoSenhaNova("1234567");
        alterarSenhaPage.preencherCampoConfirmeSenha("1234567-diferente");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("1 - Confirmaçao da nova senha está diferente da nova senha.",alterarSenhaPage.getMensagemAlerta());
    }

    @Test
    public void testSenhaNovaVazia(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("rafael.melo");
        alterarSenhaPage.preencherCampoSenhaAtual("12345");
        alterarSenhaPage.preencherCampoCPF("18773103012");
        alterarSenhaPage.preencherCampoDataNascimento("01101988");
        alterarSenhaPage.preencherCampoSenhaNova("");
        alterarSenhaPage.preencherCampoConfirmeSenha("1234567-diferente");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("1 - Confirmaçao da nova senha está diferente da nova senha.",alterarSenhaPage.getMensagemAlerta());
    }

    @Test
    public void testLoginErrado(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("teste");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("1 - O login teste inválido!",alterarSenhaPage.getMensagemAlerta());
    }

    @Test
    public void testSenhaVazia(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("rafael.melo");
        alterarSenhaPage.preencherCampoSenhaAtual("");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("1 - Senha inválida para o login",alterarSenhaPage.getMensagemAlerta());
    }

    @Test
    public void testSenhaErrada(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("rafael.melo");
        alterarSenhaPage.preencherCampoSenhaAtual("errada");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("1 - Senha inválida para o login",alterarSenhaPage.getMensagemAlerta());
    }

    @Test
    public void testDataNascimentoErrada(){
        alterarSenhaPage.acessar();
        alterarSenhaPage.preencherCampoLogin("rafael.melo");
        alterarSenhaPage.preencherCampoSenhaAtual("12345");
        alterarSenhaPage.preencherCampoCPF("18773103012");
        alterarSenhaPage.preencherCampoDataNascimento("01101989");
        alterarSenhaPage.clicarEmAlterarSenha();
        Assert.assertEquals("1 - Data de Nascimento incorreta para o login rafael.melo",
                alterarSenhaPage.getMensagemAlerta());
    }
}
