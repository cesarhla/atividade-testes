package base;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public abstract class BaseTest {

    protected WebDriver driver;

    static{
        System.setProperty("webdriver.chrome.driver", Util.workDirectory()+"/chromedriver");
    }

    public BaseTest(WebDriver driver){
        this.driver=driver;
    }


    public void basicTearDown(){
        driver.close();
        driver.quit();
    }

    public void basicSetup(){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

    }
}
