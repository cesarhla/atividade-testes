package barraca.vendas;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void acessar() {
        this.driver.get("http://barraca248.rf.gd/admin/index.php");
    }

    private WebElement getUsuarioTxt() {
        return driver.findElement(By.id("usuario"));
    }

    private WebElement getSenhaTxt() {
        return driver.findElement(By.id("senha"));
    }

    private WebElement getFazerLoginBtn(){
        return driver.findElement(By.tagName("button"));
    }

    public void preencherCampoUsuario(String login){
        this.getUsuarioTxt().sendKeys(login);
    }

    public void preencherCampoSenha(String senhaAtual){
        this.getSenhaTxt().sendKeys(senhaAtual);
    }

    public void fazerLogin(String usuario,String senha){
        this.driver.manage().deleteAllCookies();
        this.acessar();
        this.preencherCampoUsuario(usuario);
        this.preencherCampoSenha(senha);
        this.getFazerLoginBtn().click();

        WebDriverWait wait = new WebDriverWait(this.driver, 5); //seconds
        wait.until(ExpectedConditions.titleIs("Barraca248 :: Administrativo"));
    }

    public void sair(){
        this.driver.get("http://barraca248.rf.gd/admin/logout.php");
    }
}
