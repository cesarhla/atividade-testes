package barraca.vendas;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class VendasPage extends BasePage{
    public VendasPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void acessar() {
        this.driver.get(this.getEndereco());
    }

    public String getEndereco(){
        return "http://barraca248.rf.gd/admin/vendas.php";
    }

    public void navegueParaPagina(){
        this.driver.navigate().to(this.getEndereco());
    }

    private WebElement getPesquisaTxt(){
        return driver.findElement(By.id("cliente"));
    }

    private WebElement getLimpaFiltroBtn(){
        return driver.findElement(By.id("limparFiltros"));
    }

    private List<WebElement> getVendasRow(){
        return driver.findElements(By.xpath("//tbody//tr"));
    }

    private Select getComboboxQuantidadeResultados(){
        return new Select(this.driver.findElement(By.name("tabvendas_length")));
    }

    public void pesquisarVenda(String clienteOuCPF){
        this.getPesquisaTxt().sendKeys(clienteOuCPF);
    }

    public int quantidadeVendasListadas(){
        return this.getVendasRow().size();
    }

    public void limparPesquisa(){
        this.getLimpaFiltroBtn().click();
    }

    public String getTextoPesquisa(){
        return this.getPesquisaTxt().getAttribute("value");
    }

    public boolean existeMensagemSemResultado(){
        return driver.findElement(By.className("dataTables_empty")).getText().trim().equals( "Nenhum resultado encontrado!");
    }

    public void selecioneQuantidadePagina(String quantidade){
        this.getComboboxQuantidadeResultados().selectByVisibleText(quantidade);
    }
}
