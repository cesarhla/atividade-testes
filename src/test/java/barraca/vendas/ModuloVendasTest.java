package barraca.vendas;

import base.BaseTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

public class ModuloVendasTest extends BaseTest{

    private VendasPage vendasPage;
    private LoginPage loginPage;
    private String senha = "12345";
    private String usuario = "admin";

    public ModuloVendasTest() {
        super(new ChromeDriver());
        this.loginPage = new LoginPage(this.driver);
        this.vendasPage = new VendasPage(this.driver);
    }

    @Before
    public void setup(){
        this.basicSetup();
        this.loginPage.fazerLogin(usuario, senha);
    }

    @After
    public void tearDown(){
        this.loginPage.sair();
        this.basicTearDown();
    }

    @Test
    public void testPesquisarVendaPorNome(){
        vendasPage.navegueParaPagina();
        vendasPage.pesquisarVenda("francisco");
        Assert.assertTrue( vendasPage.quantidadeVendasListadas() > 0);
    }

    @Test
    public void testPesquisarVendaPorCPF(){
        vendasPage.navegueParaPagina();
        vendasPage.pesquisarVenda("11122233344");
        Assert.assertTrue( vendasPage.quantidadeVendasListadas() > 0);
    }

    @Test
    public void testPesquisarPorClienteNaoExistente(){
        vendasPage.navegueParaPagina();
        vendasPage.pesquisarVenda("kfnwqeowfnhqwuioer91374y517y873823");
        Assert.assertTrue(vendasPage.existeMensagemSemResultado());
    }

    @Test
    public void testLimparBusca(){
        String pesquisa = "te";
        vendasPage.navegueParaPagina();
        vendasPage.pesquisarVenda(pesquisa);
        Assert.assertEquals(pesquisa, vendasPage.getTextoPesquisa());

        vendasPage.limparPesquisa();
        Assert.assertEquals("", vendasPage.getTextoPesquisa());

    }

    @Test
    public void testMudarQuantidadeExibida(){
        vendasPage.navegueParaPagina();
        vendasPage.selecioneQuantidadePagina("25");
        Assert.assertEquals(25, vendasPage.quantidadeVendasListadas());

    }

}
